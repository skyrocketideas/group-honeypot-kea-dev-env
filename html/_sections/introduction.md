---
title: Introduction
# Set the display order for this section
order: 1
# Specify the layout for this section
include: sections/one-column.html
---

### Welcome to our project

Our project is based around a PHP application with a MySQL database which was developed last semester. We have used this project as a starting point as it includes the components required to assess the functionality of our development environment pipeline.

We have used Docker to containerize our application and have integrated this into a CI / CD pipeline on GitLab with version control. Our pipeline includes software quality assurance measures including linting, unit testing, code coverage analysis and documentation presentation.

The application can be deployed using a single command docker-compose up via the CLI on a remote server or locally.

- Containerization
- Software Quality Assurance
- Version Control System
- Database System
- Operating System Components

### Code examples
```

please view the full report for relevant figures

```
