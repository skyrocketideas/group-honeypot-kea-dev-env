---
title: Version Control System
order: 4
include: sections/two-column.html
---

### Gitlab

To collaborate on code and track changes throughout the project we employed version control via GitLab. Our strategy for version control was continuous integration whereby we add and commit small changes frequently and integrate into our main pipeline.

In previous projects we have setup several branches to deal with development, features, production and testing – however as we were focused on becoming familiar with continuous integration, we narrowed our branches down to dev for development and master for our main production branch.

Our strategy for version control was to individually continuously integrate to the dev branch and fix conflicts as we go. We would then merge to the master branch less frequently which would act as our production branch. This would ensure that there was a working pipeline on the master branch as much as possible.

