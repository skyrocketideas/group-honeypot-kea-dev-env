---
title: Software Quality Assurance
order: 3
include: sections/two-column.html
---

### Coding Standards

Our project is a PHP based web application. Since all of us primarily learned PHP via last semesters classes, the practices and standards that are present in the source code of the application were derived based on those experiences.

We have standardized files for most of the validation and similar needs, our naming conventions are aimed to keep everything as descriptive without excessive abbreviations as the size of the source code does not pose an issue.

### CI/CD Pipeline

Our linting, unit tests, coverage measures, and documentation presentation are integrated into a CI/CD pipeline in GitLab.

Our .gitlab-ci.yml file defines the stages of the pipeline and it functions as a configuration in the main repository relating to source code, tests, linting, version control and documentation deployment.

By integrating our workflow into GitLab we have been able to track and log issues with code and tests throughout the project. By being able to visualize these errors as they arise, it has enabled us to react quickly and provide some integrity to the building of our project.

### Linting

We have used linting tools throughout our project to analyse our code for errors and enforce coding standards. With several developers with different coding styles working on the same project, the use of linting tools is helpful to unify the codebase.

Our linting tools are initially applied to shell scripts in our static-scripts stage (see Figure 8). Firstly, we use shellcheck to analyse our shell scripts for syntax errors.

### Testing and Test Coverage

Testing primarily attempts to uphold the integrity of functionalities such as the login, signup and any kind of CRUD action issued to the database. This means that filtering errors properly is also needed as PHP is prone to flag false positives at times or errors that do not impact the functionality.

We have used unit tests in our project to aid in debugging code by isolating errors where a test fails. These tests are also a useful way for us to work together by documenting the expected output of specific tests in situ. Refactoring code is also made easier with a clear understanding of expected output.

Performing unit tests for our PHP code is handled by PHPUnit. We have implemented this through our unit-testing stage which installs dependencies required by PHPUnit from the docker_install.sh script in our ci directory.

Code coverage analysis is a useful step in the development process as it allows us to visualize how much of the source code has been executed and tested. This can help to provide a measure of reliability to the codebase and indicate which areas still have potential for weakness.

To measure the effectiveness of our unit tests, we have employed code coverage analysis using Xdebug which is part of PHPUnit. This offers the ability to execute our PHP tests and collate the results to help give us a better understanding of how deep our testing is.

With the coverage component in place, we can get percentage feedback to our pipeline indicating the amount of coverage that we are getting with our tests.
