---
title: Operating System Components
order: 6
include: sections/two-column.html
---

### Development Operating Sysytem

With the operating system (OS) being a vital link between the hardware and the application software, it’s an important consideration at the start of any project.

The OS is comprised of several layers:

- Kernel: memory management, file management, process management
- CLI: providing a way for users to interact with the OS
- GUI: a more visual way for users to interact with the OS
- Applications: editors, browsers

### Our Choice

When selecting an OS for our project it was an important to choose something robust, flexible and familiar to all team members.

We have chosen a Unix OS for both our deployment and development system due to its stability, compatibility with the internet and its ability to serve as a common ground between all developer’s regardless of the local system they are using (Windows or Mac OS in our team’s case).

Our main interaction with the OS during our project has been via the command line interface (CLI). We have chosen this method to familiarise ourselves better with shell scripting and to be able to assist each other in collaboration.

### Remote server

Our remote development server has been setup via Linode with an Alpine distribution which is a reliable and lightweight solution that offers a high degree of flexibility in customisation.

To provide a secure connection to our development and deployment servers we have setup secure shell (SSH). By providing SSH keys of authorized machines to Linode we gain extra protection against unauthorized access.

We have used VIM as our main text editor on our remote server due to its high level of customisability and flexibility. We have also incorporated TMUX as part of our profile configuration which provided us with the ability to switch between multiple terminal windows during the development process.

### Process Isolation

We have employed process isolation in our project using Docker which is similar to a virtual machine setup.

This allows us to separate different processes within the project from each other allowing us to better manage security and dependencies. By setting up our project in this way we can ensure compatibility across development machines as each container has the same base operating system and we can avoid conflicting configurations.

Our base PHP image php:8.1.0-fpm-alpine3.15 defined in our Dockerfile is built on an Alpine Linux image which we have chosen due to its small size, security, and low consumption in memory. The small size of this image helps us to achieve shorter build times during the project, as well as shortening pull times for new developers down the line.
