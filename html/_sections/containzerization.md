---
title: Containerization
order: 2
include: sections/two-column.html
---


### Docker

#### Dockerfile

We begin with a Dockerfile which is a list of instructions from which to build a Docker image. This image is then used to create a Docker container.

{:.test2}

```
FROM php:7.0-fpm
COPY ./src /app
WORKDIR /app
COPY ./data/dump.sql /docker-entrypoint-initdb.d
RUN docker-php-ext-install pdo_mysql
```

**FROM** pulls specific PHP image from Docker Hub.

**WORKDIR** defines the working directory of our container at this time.

**COPY** takes the 'src' folder and copies it into the 'app' directory in our container.

**RUN** executes our commands to install pdo_mysql which is required for our database.

#### Docker Compose

The 'Docker Compose' file is a composition of containers with run-time parameters. When the file is executed the containers are built as specified in each service.

**NGINX**

- uses the `nginx:1.11.10-alpine` image
- maps the host port 80 to the container's port 80.
- The `nginx.conf` file is copied to the containers `default.conf` file.

```
  nginx:
    image: nginx:1.11.10-alpine
    ports:
      - 80:80
    volumes:
      - ./config/nginx.conf:/etc/nginx/conf.d/default.conf
    networks:
      - webnet
    depends_on:
      - web
```
