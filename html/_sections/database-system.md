---
title: Database System
order: 5
include: sections/two-column.html
---

### MySQL

The database paradigm used for the project is relational databases, more specifically MySQL as a Relational Database Management System.

The schema consists of 5 tables, used for storing users' information, user roles, posts, likes, and password recovery data. Knowing the requirement specification for this project and that the data and the relationship between data points won’t be modified, the main option was to choose a relational database because of the organized tabular structure we need.

The database was chosen as a handy, popular, and general-purpose option, with no intention to scale the project in the first place. Moreover, we ensure that we have a very reliable system by choosing a RDBMS that is ACID compliant.
