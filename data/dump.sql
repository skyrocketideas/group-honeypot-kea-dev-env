-- MySQL dump 10.13  Distrib 8.0.26, for macos11 (x86_64)
--
-- Host: 127.0.0.1    Database: web_security
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likes` (
  `like_id` bigint unsigned NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `post_id` bigint unsigned NOT NULL,
  UNIQUE KEY `like_id` (`like_id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (62,'8403ef694f6007b1a43fc25845262ee2',1),(63,'8403ef694f6007b1a43fc25845262ee2',2),(64,'8403ef694f6007b1a43fc25845262ee2',3),(65,'8403ef694f6007b1a43fc25845262ee2',4),(66,'9b53595b9e699eb3dc5b2305249b75a0',1),(70,'d920bd9c51fa7d9d2797f664cfe5dacd',1);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_recovery`
--

DROP TABLE IF EXISTS `password_recovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_recovery` (
  `id_password_recovery` bigint unsigned NOT NULL,
  `email` text,
  `token` varchar(32) DEFAULT NULL,
  `expire` int DEFAULT NULL,
  `uuid` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_recovery`
--

LOCK TABLES `password_recovery` WRITE;
/*!40000 ALTER TABLE `password_recovery` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_recovery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `post_id` bigint unsigned NOT NULL,
  `title` text NOT NULL,
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'','Millie just got adopted'),(2,'','Extra points for the good boy who admits what he\'s done'),(3,'','Playing it real cool when dad gets back from a long business trip'),(4,'','Lorem ipsum');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `replies`
--

DROP TABLE IF EXISTS `replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `replies` (
  `reply_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `reply_text` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` varchar(32) NOT NULL,
  `post_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `replies`
--

LOCK TABLES `replies` WRITE;
/*!40000 ALTER TABLE `replies` DISABLE KEYS */;
INSERT INTO `replies` VALUES (1,'tets','2021-11-11 21:51:34','d920bd9c51fa7d9d2797f664cfe5dacd',1),(2,'test','2021-11-11 21:57:10','d920bd9c51fa7d9d2797f664cfe5dacd',3),(3,'test','2021-11-11 22:04:27','d920bd9c51fa7d9d2797f664cfe5dacd',1),(4,'tests','2021-11-11 22:08:55','d920bd9c51fa7d9d2797f664cfe5dacd',2),(5,'test','2021-11-11 22:09:22','d920bd9c51fa7d9d2797f664cfe5dacd',1),(6,'test','2021-11-11 22:09:46','d920bd9c51fa7d9d2797f664cfe5dacd',1),(7,'test','2021-11-11 22:14:20','d920bd9c51fa7d9d2797f664cfe5dacd',1),(8,'test','2021-11-11 22:17:02','d920bd9c51fa7d9d2797f664cfe5dacd',1),(9,'nice','2021-11-11 22:17:50','d920bd9c51fa7d9d2797f664cfe5dacd',1),(10,'nice','2021-11-11 22:21:31','d920bd9c51fa7d9d2797f664cfe5dacd',2),(11,'good','2021-11-11 22:34:30','d920bd9c51fa7d9d2797f664cfe5dacd',1),(12,'lorem ipsum comment','2021-11-11 22:36:39','d920bd9c51fa7d9d2797f664cfe5dacd',4),(13,'pip\r\n','2021-11-12 15:26:05','d920bd9c51fa7d9d2797f664cfe5dacd',1),(14,'yum','2021-11-12 15:30:24','d920bd9c51fa7d9d2797f664cfe5dacd',2),(15,'tasty','2021-11-12 15:31:42','d920bd9c51fa7d9d2797f664cfe5dacd',1),(16,'boom','2021-11-12 15:32:12','d920bd9c51fa7d9d2797f664cfe5dacd',3);
/*!40000 ALTER TABLE `replies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `id_user_role` bigint unsigned NOT NULL,
  `role_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'customer'),(2,'therapist');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `uuid` varchar(32) NOT NULL,
  `name` text,
  `last_name` text,
  `email` text,
  `phone` text,
  `password` text,
  `active` int DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  `verified` int DEFAULT NULL,
  `image_path` varchar(255) NOT NULL,
  `user_role` bigint unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('15f4a6891a075686a42f6e8ebbd1775b','Mohammed','Gottlieb','schinner.clyde@bode.com','15143500','$2y$10$rjpxglUVrYo1hsoKDRMKAOO3EM3Ll6EYJAF7um27xd8A7UD32vxrC',1,'649ac05af383d1ecf6326b392af04e7c',0,'default.png',1),('199c55254b93fd942c2a1afee48dbfee','Sheila','Thompson','colt.christiansen@hotmail.com','36052432','$2y$10$IB/jMOc4yOKUlxdgt4GUB.82ykSky2Ago/MiYudtmV.opJXyajLW2',1,'492c22596a9f104a7eff27d3f0940af7',0,'default.png',1),('3fc89c21d0669af47a71fc59c0e38186','Simeon','Mayer','zora.parisian@hotmail.com','61432639','$2y$10$RgSaOHi8Bifjcr5zdI3CceqVB1SW3Xh.0q2UNFVEOZMO1PpStJcb.',1,'5d5d95dea025b6f6d9bede22ea4bb6bf',0,'default.png',1),('5024e97e0899099c718445aee558177c','Daisha','Beahan','abbott.mathias@yahoo.com','19790459','$2y$10$QrwcSfIl.Bpya1AV6d7tMOyadMUzTxGPJ9ty1rxyu6yIRX.zNh0bu',1,'6594d22e7e75c8dacff1c7ea12b1b2f8',0,'default.png',1),('74550f466e1bb4b4985eeeb498c49a13','Andy','Will','thad03@connelly.com','53154044','$2y$10$oNeQXRifJxRt/ERduP/hwuieczLne58YXUID2CCB63wcCGENWH.3e',1,'9ffdddd2292151eee7cf7ce8bd1d9ebd',0,'default.png',1),('783c1207b6fe19b877331d12ca354e7b','Malachi','Muller','marshall.gleichner@hotmail.com','11420065','$2y$10$Hb4rwWczwhVpVHnK1KwWLO5LuPsyv/LUkuJ6Jri9OxiuudZmd316C',1,'6e694795e96fc9655e741a3d8438a61b',0,'default.png',1),('7942c17821e414c83f938d54b69b1879','Daniella','Braun','coralie.kuhic@hotmail.com','88964506','$2y$10$5EC/Bu5o5RdflXLWXxTQgOqxA6RXOVM2E0l57L4LCuTWVDTSBKb5.',1,'d74d44c0e6d917d35d5379242041a497',0,'default.png',1),('8403ef694f6007b1a43fc25845262ee2','John2','Doe2','onescuradu2@yahoo.com','12345679','$2y$10$jFUhz97bc0HXSQPn2ObxieUGT.xRJpkGYqG3EcUBvxML/nRBvXEBy',0,'2ea12e51fe82920c79c268901acf7d50',1,'b6dd1cb02ec3bcd7932a5a1366c1a9a0.jpeg',1),('859ff2830bfeb9dc687075b985bd33e6','therapist','exam','therapist@mail.com','22774411','$2y$10$ZaBloYybhY1H2so7XtUdR.QyRArOKNeL9VCfdY8V3dDfJ9x2F2i.e',1,'c1e73044083cf6f6748dc4f75b12d8b0',1,'384dd5ed2892206b68cc4a9c64016cb6.jpeg',2),('8804d0270a8f58315a42bb902b27b4a9','Rita','Schmidt','hill.emma@yahoo.com','72149279','$2y$10$kahugEY301lTVu7vXKZbHOJjR9cpdXmeWSAzwgEpA7OcIQ24e0Hpu',1,'22cc6c2f3e55bec7418412a0cc562490',0,'default.png',1),('8c8f3ac80dbbe084974214e68d1a7907','Suzanne','Kuhn','carmelo34@hotmail.com','82569901','$2y$10$4brmQFvnANY4MmN1HOWTr.fNT14/MRAjiohpdgdDzBuV.SxEnJnFq',1,'ffac0c9e956cb0a0053a00f625dd4c09',0,'default.png',1),('94ce41e484f04249f5ec1da41022c125','Curt','Streich','hannah.hermiston@hotmail.com','35610158','$2y$10$Uov2AQ3NNRV9TDgTRdoLP.Y3W5HLoD98QJDMVFdN6npcrR9Qhk3AS',1,'ded51eb64651629bc8a9dd867c2226db',0,'default.png',1),('9b53595b9e699eb3dc5b2305249b75a0','rodica','rotaru','examkea@gmail.com','22883366','$2y$10$kT5Jze9zJDru.E82M.CdceS/.nlsk11i281OEwC1z9sSxv4LQ2VkG',0,'a923cc00dd912e3f053f4c42665511b6',1,'f9622a5ded9f9a83fd939cd7dc9213fc.jpeg',1),('9ceb87dfdc4c209adaf93b3a7e8b5f12','test','test','test@mail.com','22335566','$2y$10$nH68zyQGIiLjeQc.ERlSv.PvoeLqWuU9bQCxll2dsp48u8u/D7mJC',1,'17b23f143e2b578fd4e19f575f1dd326',1,'ffa085076857e3267b6f1c3a00581952.png',1),('a071ea7ba469850dc81f19f9b28c2169','Alfonso','Graham','barney.jacobi@rosenbaum.com','36950049','$2y$10$uQ0w2MwebNr9MyRzzj1hpeXNMFYBJweio9ynvIdo2KbZOASTyPDy6',0,'8cf640b292dfd810e33ca7db4fd279bd',0,'default.png',1),('b2971f48ee31978749ac2184c132867d','Abner','Farrell','kyra37@mosciski.org','99808615','$2y$10$PoyANoGC8sc81h1a11Wkzu.cai0b7.vOR2.x21yGaCNFEGOQC.t9S',1,'823ab9720eb69de18e8b445b1278d5c5',0,'default.png',1),('c6caed41f591328e863965b4e8de9268','Rubie','Considine','vincenza.tillman@gmail.com','73962901','$2y$10$L3aeDOUtdXL0o5SLYZLFTe8bl8w9S/117KENfikhDZNE5yntPLM4.',1,'39838433d6068e9373a713a7df9ccfb2',0,'default.png',1),('d920bd9c51fa7d9d2797f664cfe5dacd','iulia','rotaru','irir@email.com','22334455','$2y$10$jlfVPqd9sRCg/ez08idxjO9hUvVkDwf4ZPt2F1I018yjM8kJmE2Ia',1,'8e097e6ba22ffcd7c6e95fb627f090b8',1,'9bb626253ac7bc57ed3c6fcddfd3671e.jpeg',1),('f4eacfd0cf1f0ff9621be076e9b72a41','Lon','Gottlieb','golda.langworth@oreilly.com','19404711','$2y$10$Mf7Z9nRiNE2CrNzZ9cIBf.zwlWSsfd7dqdcWgsx/lwyZg/u6KFxKC',1,'44c19b8e04a98e3aee392e9365d1b73c',0,'default.png',1),('fd02f8642e760008c4d38a430ea0ef3c','Jackeline','Rath','brendon60@yahoo.com','49339491','$2y$10$Xsp8V3.8fVKlRn7b8odRDuYD/tjtTnELQkS1dyK8BjWkf1Ja1U8rq',1,'7470038aae30d90340d3ff935cb1cd6a',0,'default.png',1),('8bde4188afdcdefecd53743c21b80a4d','dave','daveson','dave@daveson.com','65325412','$2y$10$2cyplKAf1AnIjKVde/jfju.6fSG/SMf412cxFazdVOjCS.Iq/YusS',1,'cb1196076416bf57388496537f0a28a3',0,'c93b77b4eeb5e6860fe212b2a1c23048.jpeg',1),('602f59d47b754606432ab1e736dd4f66','mike','michaels','mike@michaels.net','65452152','$2y$10$itY.vAxbyOskEuk0q6PsHusuDnqxdYETGRDn8rh6G38kRx5y.9Mv6',1,'6737ca794a560ff499d28af3c3dfa5e7',1,'6dc6df528f11126d766e142a3677750a.jpeg',1),('8fa6f5529da5a18a5ba9d6f757a146b2','graeme','redford','dave@daveson.com','65452132','$2y$10$k.Q3isH/7kOMokeGRMUw4OLNCSIY3d.r/IvCoIcuI/vqDbGN2QUxe',1,'dffe81f2506ed7b2bfed3529628c0ecc',1,'247aa3c6c3cf20e6f8d4881582312b0b.jpeg',1),('e7afe5141608ce961f59228e36853ef6','dave','davesioon','davey@dee.com','65458798','$2y$10$EMYunUePUjI27zq4sF3wzuFlUXHk9UnkYu6g8ZEh5HGPoWw82E.AO',1,'8d058bafa6b306d5da94c694a7feb4ba',1,'1d9d71ac7480f100ab25ca24be9e1568.jpeg',1),('78b78b802d303087fa0860b949f97932','mike','bobsen','mike@trike.com','65451232','$2y$10$upGFcA1exVxKO3Cv8lXGLe/uY/fUk4RMDQFhdOLqDTBqP1ZQVdKAi',1,'3c3810b4e5728fde98c6377871f1c764',1,'ac17b348437bbeab9b38f6006b50b321.jpeg',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 21:59:40
