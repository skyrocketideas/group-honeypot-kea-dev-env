#!/bin/bash

# install git - the php image doesn't have it - which is required by composer
apt-get update -yqq
apt-get install git -yqq

# install xdebug for phpunit code coverage
pecl install xdebug
docker-php-ext-enable xdebug

# install phpunit - the tool for unit testing php files
curl -k --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit-9.phar"
chmod +x /usr/local/bin/phpunit

# Here to change xdebug mode
echo xdebug.mode=coverage > /usr/local/etc/php/conf.d/xdebug.ini 
