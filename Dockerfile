FROM php:8.1.0-fpm-alpine3.15

COPY ./src /app

WORKDIR /app

RUN docker-php-ext-install pdo_mysql

