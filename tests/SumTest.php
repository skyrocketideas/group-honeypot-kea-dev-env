<?php

use PHPUnit\Framework\TestCase;

final class SumTest extends TestCase
{
public function testSum()
{
    $x = 5;
    $y = 10;
    $sum = $x + $y;
    $sum_minus = $y - $x;
    $this->assertEquals($sum, 15);
    $this->assertEquals($sum_minus, 5);
}
}
