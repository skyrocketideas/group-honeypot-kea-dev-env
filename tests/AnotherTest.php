<?php

use PHPUnit\Framework\TestCase;

final class AnotherTest extends TestCase
{
public function testSum()
{
    $x = 52;
    $y = 100;
    $sum = $x + $y;
    $sum_minus = $y - $x;
    $this->assertEquals($sum, 152);
    $this->assertEquals($sum_minus, 48);
}
}
