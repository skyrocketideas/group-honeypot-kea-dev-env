<?php

use PHPUnit\Framework\TestCase;

final class YetAnotherTest extends TestCase
{
public function testSum()
{
    $x = 3;
    $y = 8;
    $sum = $x + $y;
    $sum_minus = $y - $x;
    $this->assertEquals($sum, 11);
    $this->assertEquals($sum_minus, 5);
}
}
